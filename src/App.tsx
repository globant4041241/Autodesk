import {
  TableBody,
  TableContainer,
  TableHead,
  Paper,
  CircularProgress,
} from '@material-ui/core'
import Button from '@mui/material/Button'
import Table from '@mui/material/Table'
import TableRow from '@mui/material/TableRow'
import TableCell from '@mui/material/TableCell'
import TextField from '@mui/material/TextField'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import {
  Settings,
  ExpandMore,
  IosShare,
  FilterAltOutlined,
} from '@mui/icons-material'
import api from './service/api'
import { useEffect, useState } from 'react'
import CreateModal from './components/Modal'
import Container from '@mui/material/Container'
import Tabs from '@mui/material/Tabs'
import Tab from '@mui/material/Tab'
import { BasicExampleContext } from './context/basicExampleContext'

interface Issue {
  id: number
  status: string
  number: number
  spec: string
  rev: string
  title: string
  type: string
  priority: string
  package: string
  ballInCount: number
  dueDate: string
  response: string
}

function App() {
  const [data, setData] = useState<Issue[]>([])
  const [loading, setLoading] = useState(true)

  const listItems = () => {
    api
      .get('list')
      .then((response) => {
        setData(response.data)
        setLoading(false)
      })
      .catch((error) => {
        console.log(error)
        setLoading(false)
      })
  }

  useEffect(() => {
    listItems()
  }, [])

  //JUST FOR CONTEXTAPI EXAMPLE
  const [text, setText] = useState('')

  return (
    <BasicExampleContext.Provider value={{ text, setText }}>
      <Typography
        variant="h3"
        sx={{ borderBottom: 1, borderColor: 'grey.500', pb: 1, mb: 2 }}
      >
        MAIN PAGE:
      </Typography>
      <Container sx={{ p: 3 }}>
        <Typography variant="h4" sx={{ pb: 1 }}>
          Submittals
        </Typography>

        <Box sx={{ borderBottom: 1, borderColor: 'divider', mb: 2 }}>
          <Tabs value={0} aria-label="basic tabs example">
            <Tab label="Items" />
            <Tab label="Packages" />
          </Tabs>
        </Box>

        {loading ? (
          <div>
            <CircularProgress />
          </div>
        ) : (
          <>
            <Box sx={{ display: 'flex', mb: 2 }}>
              <CreateModal />
              <Box sx={{ flexGrow: 1, textAlign: 'end' }}>
                <Button
                  variant="outlined"
                  startIcon={<IosShare />}
                  endIcon={<ExpandMore />}
                  sx={{ pt: '7px', pb: '6px' }}
                >
                  Export
                </Button>
                <TextField
                  size="small"
                  sx={{ minWidth: '320px', mx: 2 }}
                  id="search"
                  label="Search by title, spec or ball in count"
                  variant="outlined"
                />
                <Button
                  variant="outlined"
                  sx={{ pt: '7px', pb: '6px', minWidth: 0 }}
                >
                  <FilterAltOutlined />
                </Button>
              </Box>
            </Box>
            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>Status</TableCell>
                    <TableCell align="right">#</TableCell>
                    <TableCell align="right">Spec</TableCell>
                    <TableCell align="right">Rev</TableCell>
                    <TableCell align="right">Title</TableCell>
                    <TableCell align="right">Type</TableCell>
                    <TableCell align="right">Priority</TableCell>
                    <TableCell align="right">Package</TableCell>
                    <TableCell align="right">Ball in count</TableCell>
                    <TableCell align="right">Due date </TableCell>
                    <TableCell align="right">Response</TableCell>
                    <TableCell align="right">
                      <Settings />
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {data &&
                    data.map((row) => (
                      <TableRow
                        key={row.id}
                        sx={{
                          '&:last-child td, &:last-child th': { border: 0 },
                        }}
                      >
                        <TableCell>{row.status}</TableCell>
                        <TableCell align="right">{row.number}</TableCell>
                        <TableCell align="right">{row.spec}</TableCell>
                        <TableCell align="right">{row.rev}</TableCell>
                        <TableCell align="right">{row.title}</TableCell>
                        <TableCell align="right">{row.type}</TableCell>
                        <TableCell align="right">{row.priority}</TableCell>
                        <TableCell align="right">{row.package}</TableCell>
                        <TableCell align="right">{row.ballInCount}</TableCell>
                        <TableCell align="right">{row.dueDate}</TableCell>
                        <TableCell
                          align="right"
                          sx={
                            row.response === 'Approved'
                              ? { color: '#AAFF00' }
                              : { color: '#FF0000' }
                          }
                        >
                          {row.response}
                        </TableCell>
                        <TableCell align="right"></TableCell>
                      </TableRow>
                    ))}
                </TableBody>
              </Table>
            </TableContainer>
          </>
        )}
      </Container>
    </BasicExampleContext.Provider>
  )
}

export default App
