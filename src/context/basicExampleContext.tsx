import { createContext } from 'react'

interface BasicExampleContextType {
  text: string
  setText: React.Dispatch<React.SetStateAction<string>>
}

export const BasicExampleContext = createContext<BasicExampleContextType>({
  text: '',
  setText: () => '',
})
