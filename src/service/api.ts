import axios from 'axios'

const api = axios.create({
  //baseURL: 'https://autodesktest.free.beeceptor.com/',
  baseURL: 'http://demo6827846.mockable.io/',
})

export default api
