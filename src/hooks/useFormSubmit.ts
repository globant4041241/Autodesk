import { useState } from 'react'
//THiS COMPONENT IS JUST FOR SHOW AN EXAMPLE OF HOW TO CRIATE A CUSTOM HOOK

export function useFormSubmit() {
  const [isFormSubmitting, setIsFormSubmitting] = useState(false)

  return { isFormSubmitting, setIsFormSubmitting }
}
