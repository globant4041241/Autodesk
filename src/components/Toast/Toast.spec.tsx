/** @vite-environment jsdom */

import { render, screen } from '@testing-library/react'
import Toast from '.'
import { AlertColor } from '@mui/material'

interface ToastProps {
  type: AlertColor
  msg: string
  open: boolean
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

describe('Toast component', () => {
  it('should render a Snackbar component with the correct props', () => {
    const props: ToastProps = {
      type: 'success',
      msg: 'This is a toast message.',
      open: true,
      setOpen: () => {},
    }
    render(<Toast {...props} />)
    const snackbar = screen.queryByRole('alert')
    expect(snackbar).toBeTruthy()
  })
})
