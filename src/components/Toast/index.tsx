import Snackbar from '@material-ui/core/Snackbar'
import Alert, { AlertColor } from '@mui/material/Alert'
export interface ToastProps {
  type: AlertColor
  msg: string
  open: boolean
  setOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export default function Toast({
  type = 'success',
  msg,
  open = false,
  setOpen,
}: ToastProps) {
  return (
    <Snackbar
      open={open}
      autoHideDuration={6000}
      onClose={() => setOpen(false)}
      anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
    >
      <Alert
        onClose={() => setOpen(false)}
        severity={type}
        sx={{ width: '100%' }}
      >
        {msg}
      </Alert>
    </Snackbar>
  )
}
