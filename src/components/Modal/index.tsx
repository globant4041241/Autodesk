import { useContext, useState } from 'react'
import Modal from '@mui/material/Modal'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import FormControlLabel from '@mui/material/FormControlLabel'
import Checkbox from '@mui/material/Checkbox'
import { SelectChangeEvent } from '@mui/material/Select'
import Typography from '@mui/material/Typography'
import Box from '@mui/material/Box'
import Button from '@mui/material/Button'
import { Add, Close } from '@mui/icons-material'
import Toast from '../Toast'
import { AlertColor } from '@mui/material/Alert'
import { useMediaQuery, useTheme } from '@material-ui/core'
import { BasicExampleContext } from '../../context/basicExampleContext'
import { Field, Form, Formik } from 'formik'
import { TextField, Select } from 'formik-mui'
import * as Yup from 'yup'
import LinearProgress from '@mui/material/LinearProgress'
import api from '../../service/api'
import { useFormSubmit } from '../../hooks/useFormSubmit'

export default function CreateModal() {
  const [spec, setSpec] = useState<string>('')
  const [type, setType] = useState<string>('')
  const [open, setOpen] = useState(false)
  const [showToast, setShowToast] = useState(false)
  const [toastMsg, setToastMsg] = useState('')
  const [toastType, setToastType] = useState<AlertColor>('success')
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const theme = useTheme()
  const isSmallScreen = useMediaQuery(theme.breakpoints.down('sm'))
  const { isFormSubmitting, setIsFormSubmitting } = useFormSubmit()

  const modalStyle = {
    position: 'absolute',
    top: '43%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: isSmallScreen ? '90%' : '50%',
    height: '70%',
    margin: isSmallScreen ? '10px' : '50px',
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    overflow: 'auto',
  }

  const inputStyle = {
    marginTop: 2,
    width: '100%',
  }

  const handleChangeSpec = (event: SelectChangeEvent) => {
    setSpec(event.target.value as string)

    //JUST FOR EXAMPLE CONTEXTAPI
    setText(event.target.value)
  }

  const handleChangeType = (event: SelectChangeEvent) => {
    setType(event.target.value as string)
  }

  //JUST FOR CONTEXTAPITEST
  const { text, setText } = useContext(BasicExampleContext)

  const initialValues = {
    spec: '',
    subspec: '',
    title: '',
    description: '',
    type: '',
  }

  const schema = Yup.object().shape({
    spec: Yup.string().required(),
    title: Yup.string().required(),
    type: Yup.string().required(),
  })

  return (
    <>
      <Toast
        msg={toastMsg}
        open={showToast}
        setOpen={setShowToast}
        type={toastType}
      />
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={modalStyle}>
          <Formik
            initialValues={initialValues}
            onSubmit={(values) => {
              setIsFormSubmitting(true)
              api
                .post('item', {
                  spec: spec,
                  subspec: values.subspec,
                  title: values.title,
                  description: values.description,
                  type: type,
                })
                .then(function (response) {
                  setShowToast(true)
                  setToastMsg('Successfully created the item')
                  setToastType('success')
                  handleClose()
                  setIsFormSubmitting(true)
                  console.log('response', response.data)
                })
                .catch(function (error) {
                  setShowToast(true)
                  setToastMsg(error.message)
                  setToastType('error')
                  setIsFormSubmitting(false)
                })

              //JUST FOR CONTEXTAPI TEST
              console.log(text)
            }}
            validationSchema={schema}
          >
            {({ submitForm }) => (
              <Form>
                <Box
                  sx={{
                    borderBottom: 1,
                    borderColor: 'grey.500',
                    p: 3,
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <Typography
                    id="modal-modal-title"
                    variant="h6"
                    component="h2"
                  >
                    Create Item
                  </Typography>
                  <Button sx={{ minWidth: 0 }} onClick={handleClose}>
                    <Close />
                  </Button>
                </Box>
                <Box sx={{ px: 4 }}>
                  <Box sx={{ px: 3, pt: 3 }}>
                    <Box
                      sx={{ display: 'flex', justifyContent: 'space-between' }}
                    >
                      <Typography
                        variant="caption"
                        sx={{ display: 'block', pb: 2 }}
                      >
                        General Info
                      </Typography>

                      <Button>Create new</Button>
                    </Box>
                    <FormControl fullWidth>
                      <Field
                        component={Select}
                        labelId="spec"
                        id="spec"
                        name="spec"
                        value={spec}
                        label="Spec section"
                        onChange={handleChangeSpec}
                      >
                        <MenuItem value="Spec 1">Spec 1</MenuItem>
                        <MenuItem value="Spec 2">Spec 2</MenuItem>
                        <MenuItem value="Spec 3">Spec 3</MenuItem>
                      </Field>
                    </FormControl>
                  </Box>
                  <Box sx={{ px: 3 }}>
                    <Field
                      sx={{ mt: 2 }}
                      component={TextField}
                      id="subspec"
                      label="Sub Spec section"
                      variant="outlined"
                      name="subspec"
                    />
                    <Field
                      sx={inputStyle}
                      component={TextField}
                      id="title"
                      label="Title*"
                      variant="outlined"
                      name="title"
                    />
                    <Field
                      sx={{ my: 3, width: '100%' }}
                      component={TextField}
                      id="description"
                      label="Description"
                      variant="outlined"
                      multiline
                      name="description"
                      rows={4}
                    />
                  </Box>

                  <Box sx={{ px: 3 }}>
                    <FormControl fullWidth>
                      <Field
                        component={Select}
                        labelId="type"
                        id="type"
                        value={type}
                        name="type"
                        label="Type*"
                        onChange={handleChangeType}
                      >
                        <MenuItem value="type 1">type 1</MenuItem>
                        <MenuItem value="type 2">type 2</MenuItem>
                        <MenuItem value="type 3">type 3</MenuItem>
                      </Field>
                    </FormControl>
                  </Box>
                </Box>
                <Box
                  sx={{
                    borderTop: 1,
                    borderColor: 'grey.500',
                    p: 3,
                    mt: 3,
                    display: 'flex',
                    justifyContent: 'space-between',
                  }}
                >
                  <FormControlLabel
                    control={<Checkbox />}
                    label="Create another"
                  />
                  <Box>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button
                      variant="contained"
                      disabled={isFormSubmitting}
                      disableElevation
                      onClick={submitForm}
                    >
                      Create
                    </Button>
                    {isFormSubmitting && <LinearProgress color="secondary" />}
                  </Box>
                </Box>
              </Form>
            )}
          </Formik>
        </Box>
      </Modal>

      <Button onClick={handleOpen} variant="contained" startIcon={<Add />}>
        Create Item
      </Button>
    </>
  )
}
