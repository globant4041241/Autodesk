# Autodesk Knowledge Assessment Project

This project is a simple React application that displays a list of items from a mock API endpoint. The user can also create a new item in the list by opening a modal.

## Technologies

- React
- Vite
- Axios
- Vitest
- Testing Library
- TypeScript

## How to Run

1. Clone the repository.
2. Install dependencies with `yarn install`.
3. Run the command `yarn dev` to start the development server.
4. Check the terminal link for open in the browser.

## How to Test

Run the command `yarn test` to run the tests and `yarn coverage` to see coverage.
